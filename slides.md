class: center, middle

.presentationtitle[
# What is version control <br>and why should I care?
]

Renato Alves - EMBL Heidelberg

BTM - October 2016

[https://git.embl.de/ralves/what_is_version_control](https://git.embl.de/ralves/what_is_version_control)

---

# Setting up

We'll use [http://ralves.embl.de:8888](http://ralves.embl.de:8888) again.

And please also login on [https://git.embl.de](https://git.embl.de).

---

# What is version control?

**Version** - things that change over time

--

<br>

.center[![:scale 100%](content/poor-mans-git.png)]

.credits[by [Luis Pedro Coelho](luispedro.org)]

---

# What is version control?

**Control** - well...

--

.center[![:scale 50%](content/version_control.png)]

.credits["Piled Higher and Deeper" by Jorge Cham, [PhD Comics](www.phdcomics.com)]

---

# Why should I care?

* Historical record of your actions
    * Ability to travel back in time
    * Compare past with present
    * Figure out when things stopped working and why
* Complement to your lab-book
* Easier collaboration

--

* Poor man's backup

--

* Laugh at how bad your past self was at it
* Learn how to be better to your future self

---

# Introducing Git

* [Git](https://git-scm.com) started as something to manage the code behind the Linux kernel.
* Rapidly became the most popular tool for the job
    * Obsoleting: CVS, [Subversion (aka SVN)](https://subversion.apache.org)
    * Alternative: [Mercurial (aka hg)](https://www.mercurial-scm.org)
* Distributed model - full copy of entire history
* Allows for *offline work*, something not possible with CVS and SVN

--

# Use it for

* Code. That's where it all started...
* Anything text...
* Images, audio, video & other binary ¹

.footnote[¹ With some caveats]

---

# Things to be aware of

* Git tracks things over time
    * meaning repositories can only get bigger

--

* Really big files?
    * 1GB+ and you will notice the effect.
    * Possible solutions:
        * [git-annex](https://git-annex.branchable.com/) - Store large files externally to git
        * [LFS](https://git-lfs.github.com/) - similar to git-annex, by Github

---

# So how does it work?

* Consider one of your projects:

```nohighlight
myprojects/why_are_bananas_curved
```

* Git tracks content under a *project* folder.
  * Tell it to track the project and it will inform about new and changed content.

--

```nohighlight
% cd myprojects/why_are_bananas_curved

% git init

% git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        literature_review.txt
        experiment_1/
```

---

# Using git

## Git concepts

* `commit` - the unit of work you ask git to save
  * This can be anything from a change in a single line to an entire new folder with several files in it.
  * One of the commands you will use the most

--

* `branch` - the line of work
  * Think of it as 2 collaborators, 2 different versions, 2 branches

--

* `merge` - the action of fusing 2 branches into one
  * Such as merging comments from 2 collaborators into a single file

--

* `conflict` - when the same content is changed in different ways in two branches
  * 2 collaborators, different corrections to the same part of text
  * Seen mostly during merges.

---

## Git concepts

* `clone` - full copy of a git repository and its history

--

* `push` & `pull` - sending/receiving changes to/from other people
  * Used to interact with other linked git repositories, aka `clones`

--

* `tag` - alias added to a commit (something you can remember)
  * Like *submitted to nature - 1st review* or *1st working experiment* ...

???

A little explanation of the git model, the SHA1 and the graph of commits


--

* `diff` & `patch` - summary of differences between two versions
  * works best if content is all text

--

## Github & friends concept

* `pull request` - the action of asking someone to `pull` changes from your copy of their repository.

---

# Using git

Via the web:

[Gitlab](https://git.embl.de) & [Github](https://github.com)

--

The command line:

```bash
% cd myproject

# Only necessary the first time
% git init

% git add *
% git commit -m "My first commit"
```
--

Or [one of the many graphical interfaces](https://git-scm.com/downloads/guis)

---

# Some hands on

[Gitlab](https://git.embl.de) & Terminal on [http://ralves.embl.de:8888](http://ralves.embl.de:8888).

## Exercises

.margin10[

* Create a **text file** with Jupyter on your folder.
    * `git init` your folder to set it up as a git repository
    * `git add` the file
    * `git commit` with a description
    * Modify the file again
    * `git status` and check what's going on
    * `git commit` again to add a new record with the changes
    * `git log` to see the history of your changes.
    * Create a new public repository on `git.embl.de` and try to `git push` to it following the instructions provided on the website.
    * Ask a person sitting next to you if they can `git clone` your repository and change it like they have so far.

* Try to `git clone` one of the public repositories available on git.embl.de
]

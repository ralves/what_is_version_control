.PHONY: list run update update-dev
list:
	@echo "Available options:"
	@echo
	@echo "make run    - run a local http server"
	@echo "make update - update remark.js to the latest version"
	@echo

run:
	@python3 -m http.server

update:
	@mv -f static/js/remark-latest.min.js static/js/remark-latest.min.js.old
	@wget https://gnab.github.io/remark/downloads/remark-latest.min.js -O static/js/remark-latest.min.js
